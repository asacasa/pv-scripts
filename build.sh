#!/bin/bash

set -e

export PANTAVISOR_VERSION="003"
export PANTAVISOR_DEBUG=${PANTAVISOR_DEBUG:-no}

TARGET=$1
export TARGET

TOP_DIR=$(cd $(dirname $0) && pwd -P)

cpus=`cat /proc/cpuinfo | grep processor | wc -l`
threads=$(($cpus + 1))

if test -z "$MAKEFLAGS"; then
	MAKEFLAGS="-j$threads"
fi

export MAKEFLAGS

target=
uboot=

udev_update() {
	if test -n "${IMG_DEVICE}"; then
		udevadm trigger
		udevadm settle
	fi
}

setup_kernel_atom() {
	find "$TOP_DIR/kernel/" -iname "atom.mk" | xargs rm -f
	if [ ! -f "$TOP_DIR/kernel/$kernel/atom.mk" ]; then
		echo "Setting up $kernel kernel"
		ln -s "$TOP_DIR/scripts/atoms/kernel-mk" "$TOP_DIR/kernel/$kernel/atom.mk"
	fi
}

setup_uboot() {
	find "$TOP_DIR/bootloader/" -iname "atom.mk" | xargs rm -f
	if [ ! -z "$uboot" ]; then
		if [ ! -f "$TOP_DIR/bootloader/$uboot/atom.mk" ]; then
			echo "Setting up $uboot bootloader"
			ln -s "$TOP_DIR/scripts/atoms/uboot-mk" "$TOP_DIR/bootloader/$uboot/atom.mk"
		fi
	fi
}

setup_alchemy () {
	export ALCHEMY_HOME=${TOP_DIR}/alchemy
	export ALCHEMY_WORKSPACE_DIR=${TOP_DIR}

	export ALCHEMY_TARGET_PRODUCT=traild
	export ALCHEMY_TARGET_PRODUCT_VARIANT=$target
	export ALCHEMY_TARGET_CONFIG_DIR=${TOP_DIR}/config/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	export ALCHEMY_TARGET_OUT=${TOP_DIR}/out/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	export ALCHEMY_USE_COLORS=1

	export TRAIL_BASE_DIR=${ALCHEMY_TARGET_OUT}/trail/
	export TARGET_VENDOR_DIR=${TOP_DIR}/vendor/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	export PVR="${TOP_DIR}/scripts/pvr/pvr"
	if [ "${PANTAVISOR_DEBUG}" == no ]; then
		export KCPPFLAGS='-DENV_DEVICE_SETTINGS_EXTRA=\"silent=1\\0\"'
	fi
}

build_mmc_tools() {
	${ALCHEMY_HOME}/scripts/alchemake host.e2fsprogs
	${ALCHEMY_HOME}/scripts/alchemake host.mtools
}

build_mmc_image() {
	export DEBUGFS=${ALCHEMY_TARGET_OUT}/staging-host/usr/sbin/debugfs
	POPULATEEXTFS=${ALCHEMY_TARGET_OUT}/staging-host/bin/populate-extfs.sh
	MCOPY=${ALCHEMY_TARGET_OUT}/staging-host/usr/bin/mcopy
	if test ! -e $MCOPY; then
		echo cannot find mcopy from mtools to generate vfat partition
		exit 36
	fi
	if test ! -e $POPULATEEXTFS; then
		echo cannot find populate-extfs.sh script to build trail storage
		exit 37
	fi
	PI=$target
	if [ -e config/${target}/image.config ]; then
		source config/${target}/image.config
	fi

	echo making base mmc image
	tmpimg=${IMG_DEVICE:-`mktemp`}
	if test -z "$IMG_DEVICE" -o -n "$IMG_DEVICE_CLEAN" && [ -n "$MMC_SIZE" ]; then
		dd if=/dev/zero of=$tmpimg bs=1M count=0 seek=$MMC_SIZE
		sync $tmpimg
	elif [ -n "$IMG_DEVICE" ]; then
		dd if=/dev/zero of=$tmpimg bs=1M count=1
		sync $tmpimg
	else
		tmpimg=
	fi
	if test $? -ne 0; then
		echo error creating disk image
		exit 5
	fi
	echo making boot part
	if [ -n "$tmpimg" ] && [ "$PI" == "bbb" ]; then
		dd if=${TOP_DIR}/out/${target}/build/uboot/MLO of=$tmpimg bs=512 seek=256 count=256 conv=notrunc
		dd if=${TOP_DIR}/out/${target}/build/uboot/u-boot.img of=$tmpimg bs=512 seek=768 count=1024 conv=notrunc
		cp ${TOP_DIR}/out/${target}/build/linux/arch/arm/boot/dts/am335x-boneblack.dtb ${TOP_DIR}/out/${target}/trail/final/trails/0/.pv
	fi

	if [ -z "$tmpimg" ]; then
		echo "building root tarball, not image. skipping preparing the boot partition ..."
	elif [ "$PI" == "rpi3" ] || [ "$PI" == "rpi4" ] || [ "$PI" == "rpi0w" ] || [ "$PI" == "bbb" ]; then
		parted -s $tmpimg -- mklabel msdos \
			mkpart p fat32 1MiB $(($BOOT_SIZE + 1))MiB
	elif [ "$PI" == "bpi-r2" ]; then
		parted -s $tmpimg -- mklabel msdos \
			mkpart p fat32 100MiB $(($BOOT_SIZE + 100))MiB
		cp -f $tmpimg ${TOP_DIR}/out/backup1
	else
		parted -s $tmpimg -- mklabel gpt \
			mkpart ESP fat32 1MiB $(($BOOT_SIZE + 1))MiB \
			set 1 boot on
	fi

	if test $? -ne 0; then
		echo error partitioning image file
		exit 6
	fi
	sync $tmpimg
	udev_update

	tmpfs=

	if [ -n "$BOOT_SIZE" ]; then
		tmpfs=`mktemp`
		echo making vfat boot fs image with size ${BOOT_SIZE}MiB
		dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$BOOT_SIZE
		mkfs.vfat -n pvboot $tmpfs
		echo copying boot contents to \"$tmpfs\"
		if [ -d "${TOP_DIR}/vendor/${target}/boot/" ]; then
			$MCOPY -v -i $tmpfs -s ${TOP_DIR}/vendor/${target}/boot/* ::/
		fi
		if echo $TARGET | grep -q -- -installer; then
			$MCOPY -v -i $tmpfs -s ${TOP_DIR}/out/${target}/trail/final/trails/0/.pv/pv-kernel.img ::/pv-kernel.img
		fi
		if [ -d "${TOP_DIR}/out/${target}/final/boot/" ]; then
			$MCOPY -i $tmpfs -s ${TOP_DIR}/out/${target}/final/boot/u-boot.bin ::/uboot.bin
			if [ "${PANTAVISOR_DEBUG}" == "no" ]; then
				echo "INSTALLING PRODUCTION UBOOT ENV"
				if [ -e ${TOP_DIR}/config/${target}/boot.scr ]; then
					$MCOPY -i $tmpfs -s ${TOP_DIR}/config/${target}/boot.scr ::/boot.scr
				else
					$MCOPY -i $tmpfs -s ${TOP_DIR}/config/${target}/uboot.env ::/uboot.env
				fi
			else
				echo "INSTALLING DEBUG UBOOT ENV (if available)"
				if [ -e ${TOP_DIR}/config/${target}/boot.debug.scr ]; then
					$MCOPY -i $tmpfs -s ${TOP_DIR}/config/${target}/boot.debug.scr ::/boot.scr
				elif [ -e ${TOP_DIR}/config/${target}/boot.scr ]; then
					$MCOPY -i $tmpfs -s ${TOP_DIR}/config/${target}/boot.scr ::/boot.scr
				elif [ -e ${TOP_DIR}/config/${target}/uboot.debug.env ]; then
					$MCOPY -i $tmpfs -s ${TOP_DIR}/config/${target}/uboot.debug.env ::/uboot.env
				elif [ -e ${TOP_DIR}/config/${target}/uboot.env ]; then
					$MCOPY -i $tmpfs -s ${TOP_DIR}/config/${target}/uboot.env ::/uboot.env
				fi
			fi
		fi
		bootfs=$tmpfs
		sync $bootfs
	fi

	# if we have a boot-installer directory in vendor/ we will overlay it and use that for the actual
	# installer image ...
	if (echo $TARGET | grep -q -- -installer) && [ -d ${TOP_DIR}/vendor/${target}/boot-installer ]; then
		echo "making installer fat partition 'pvfboot'"
		tmpfs=`mktemp`
		cp $bootfs $tmpfs
		fatlabel $tmpfs pvfboot
		$MCOPY -D o -v -i $tmpfs -s ${TOP_DIR}/vendor/${target}/boot-installer/* ::/
		sync $tmpfs
	fi

	echo writing boot fs to disk image part 1
	if [ -n "$tmpimg" ]; then
		if [ "$PI" == "bpi-r2" ]; then
			# bpi-r2 has a 100MB no-partition offset for bootloader and stuff
			dd conv=notrunc if=$tmpfs of=$tmpimg bs=1M seek=100
		else
			dd conv=notrunc if=$tmpfs of=$tmpimg bs=1M seek=1
		fi
		sync $tmpimg
		echo boot fs written to disk image part 1

		if [ "$PI" == "bpi-r2" ]; then
			size_i=$(($BOOT_SIZE + 100))
		else
			size_i=$(($BOOT_SIZE + 1))
		fi

		part_i=2
		for part_size in ${MMC_OTHER_PART_SIZES}; do
			echo making ext4 data fs image with size ${part_size}MiB
			tmpfs=`mktemp`
			dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$part_size
			fakeroot mkfs.ext4 -L pvol$part_i $tmpfs
			sync $tmpfs
			seek=$(($size_i * 1024))
			echo writing other part fs to disk image part $part_i with seek=${seek}KiB
			parted -s $tmpimg -- \
				${MMC_OTHER_MKPART}
			dd conv=notrunc if=$tmpfs of=$tmpimg bs=1K seek=$seek
			sync $tmpimg
			size_i=$(($size_i + $part_size))
			part_i=$(($part_i + 1))
			rm -f $tmpfs
		done
	fi

	_root_label=pvroot
	_root_src_dir=${TOP_DIR}/out/${target}/trail/final/
	_delete_dirs=
	if echo $TARGET | grep -q -- -installer; then
		_root_label=pvdata
		_orig_src_dir=$_root_src_dir
		_root_src_dir=`mktemp -d -t pvinstall-data.XXXXXX`
		_delete_dirs="$delete_dirs $_root_src_dir"

		# install files in vendor pvfactory dir
		cp -rf ${TOP_DIR}/vendor/${target}/pvfactory/* $_root_src_dir/

		# local pvfactory env; use for vendor settings/credentials etc.
		if [ -n "$PV_FACTORY_LOCAL" -a -d "$PV_FACTORY_LOCAL" ]; then
			cp -rfv ${PV_FACTORY_LOCAL}/* $_orig_src_dir/
		fi

		# XXX TODO
		tmpf=`mktemp`
		sha256sum -b ${TOP_DIR}/vendor/${target}/boot/grub/grub.cfg | awk '{ print $1 }' > $tmpf
		cp -f $tmpf $_root_src_dir/config/grub.cfg.sha256

		# pack up root
		fakeroot tar -C $_orig_src_dir -cvJf $_root_src_dir/root.tar.xz .

		# pack up boot
		cp -f $bootfs $_root_src_dir/boot.bin.$BOOT_SIZE
	fi
	rm -f $bootfs


	if [ -z "$tmpimg" ]; then
		echo "making root as tarball."
		fakeroot tar -C $_root_src_dir -cvJf out/$target/${TARGET}-rootfs.tar.xz .
	elif test -z "$IMG_DEVICE"; then
		DATA_SIZE=$(($MMC_SIZE - $size_i - 1))
		SEEK_K=$(($size_i*1024))
		echo making ext4 data fs image for pv storage ${DATA_SIZE}MiB
		tmpfs=`mktemp`
		dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$DATA_SIZE
		fakeroot mkfs.ext4 -L $_root_label $tmpfs
		sync $tmpfs
		echo copying trail storage data to \"$tmpfs\"
		fakeroot $POPULATEEXTFS $_root_src_dir $tmpfs
		sync $tmpfs
		echo writing trail data fs to disk image part 2 with seek in KiB=$SEEK_K
		parted -s $tmpimg -- \
			mkpart p ext4 ${size_i}MiB -1MiB
		dd conv=notrunc if=$tmpfs of=$tmpimg bs=1K seek=${SEEK_K}
		sync $tmpimg
		echo trail data fs written to disk image part 2
		rm -f $tmpfs
	else
		echo making ext4 data fs on device ${IMG_DEVICE}${part_i}
		parted -s ${IMG_DEVICE} -- \
			mkpart p ext4 ${size_i}MiB 100%
		udev_update
		fakeroot mkfs.ext4 -L pvroot ${IMG_DEVICE}${part_i}
		sync
		mntp=`mktemp -d`
		mount ${IMG_DEVICE}${part_i} $mntp
		tar -C $_root_src_dir -c . | tar -C $mntp -xv
		umount $mntp
		rmdir $mntp
		sync $IMG_DEVICE
	fi

	if [ -n "$_delete_dirs" ]; then
		for d in $_delete_dirs; do
			rm -rf $d
		done
	fi

	if test -z "$IMG_DEVICE" && test -n "$tmpimg" ; then
		if [ "$PI" == "bpi-r2" ]; then
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD440-0k.img of=$tmpimg conv=notrunc
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD1-512b.img of=$tmpimg bs=512 seek=1 conv=notrunc
			gunzip -c ${TOP_DIR}/vendor/${target}/SD/BPI-R2-720P-2k.img.gz | dd of=$tmpimg bs=1024 seek=2 conv=notrunc
			if [ -f ${TOP_DIR}/out/${target}/final/boot/u-boot.bin ]; then
				 cat ${TOP_DIR}/out/${target}/final/boot/u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			elif [ -f ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin ]; then
				 cat ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			fi
		fi

		mv $tmpimg out/$target/${TARGET}-pv-${MMC_SIZE}MiB.img
		echo -e "\nmmc image avaialble at out/$target/${TARGET}-pv-${MMC_SIZE}MiB.img"
		echo please flash onto ${PI} sd card with dd

	elif test -n "$IMG_DEVICE"; then
		if [ "$PI" == "bpi-r2" ]; then
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD440-0k.img of=$IMG_DEVICE conv=notrunc
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD1-512b.img of=$IMG_DEVICE bs=512 seek=1 conv=notrunc
			gunzip -c ${TOP_DIR}/vendor/${target}/SD/BPI-R2-720P-2k.img.gz | dd of=$IMG_DEVICE bs=1024 seek=2 conv=notrunc
			if [ -f ${TOP_DIR}/out/${target}/final/boot/u-boot.bin ]; then
				 cat ${TOP_DIR}/out/${target}/final/boot/u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			elif [ -f ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin ]; then
				 cat ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			fi
		fi
		echo -e "\nmmc image flashed on block device ${IMG_DEVICE}"
	fi

}

case $TARGET in
arm-bpi-r2)
	export LOADADDR=0x80008000
	target="bpi-r2"
	kernel="linux-bpi-r2-owrt"
	uboot="bpi-r2"
	;;
arm-qemu)
	target="vexpress-a9"
	kernel="vexpress-a9"
	uboot="vexpress-a9"
	;;
malta-qemu)
	target="malta"
	kernel="malta"
	uboot="malta"
	;;
legacy-qemu)
	export BL_IS_PVK="yes"
	target="legacy"
	kernel="malta"
	uboot="malta"
	;;
mips-mt300a)
	export PV_NO_UBOOT=1
	export PV_BL_IS_PVK="yes"
	target="mt300a"
	kernel="mt300a"
	;;
mipsel)
	target="mipsel"
	;;
mips-generic)
	target="mips-generic"
	kernel=""
	uboot=""
	;;
arm-generic)
	target="arm-generic"
	kernel=""
	uboot=""
	;;
arm-rpi0w-mmc)
	target="rpi0w"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi0w)
	export LOADADDR=0x00008000
	target="rpi0w"
	kernel="rpi-4.14.y"
	uboot="rpi3"
	;;
arm-rpi2-mmc)
	target="rpi2"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi2)
	target="rpi2"
	kernel="rpi3"
	uboot="rpi3"
	;;
arm-rpi3-mmc)
	target="rpi3"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi3)
	export LOADADDR=0x00008000
	target="rpi3"
	kernel="rpi3"
	uboot="rpi3"
	;;
arm-rpi4-mmc)
	target="rpi4"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi4)
	export LOADADDR=0x00008000
	target="rpi4"
	kernel="rpi3"
	uboot="rpi4"
	;;
arm-bbb)
	export LOADADDR=0x80008000
	target="bbb"
	kernel="bbb"
	uboot="bbb"
	;;
arm-mvebu)
	export LOADADDR=0x00008000
	target="mvebu"
	kernel="linux-openwrt-xp"
	uboot=""
	;;
arm64-hikey)
	target="hikey"
	kernel="hikey"
	;;
x64-uefi)
	target="x64-uefi"
	kernel="linux-stable"
	;;
x64-uefi-installer)
	target="x64-uefi"
	kernel="linux-stable"
	;;
*)
	echo "Must define target product as first argument [arm-qemu, malta-qemu, arm-generic, mips-generic, arm-mvebu, arm-rpi3, arm-rpi2, arm-rpi4, x64-uefi]"
	exit 1
	;;
esac

setup_alchemy

if test -z "$PANTAHUB_HOST"; then
	PANTAHUB_HOST=api.pantahub.com
fi
if test -z "$PANTAHUB_PORT"; then
	PANTAHUB_PORT=443
fi


if [ ! -z "$2" ]; then
	if [ "$2" == "upload" ]; then
		cd $TRAIL_BASE_DIR/staging
		$PVR putobjects -f https://$PANTAHUB_HOST:$PANTAHUB_PORT/objects
		cd $TOP_DIR
	else
		${ALCHEMY_HOME}/scripts/alchemake "${@:2}"
	fi
elif [ "$target" == "malta" -o "$target" == "vexpress-a9" -o "$target" == "legacy" -o "$target" == "mt300a" ]; then
	echo "Building $target target"
	setup_kernel_atom
	setup_uboot
	if [ ! -f ${TOP_DIR}/out/$target/build-host/qemu/qemu.done ]; then
		${ALCHEMY_HOME}/scripts/alchemake host.qemu
	fi
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail
	${ALCHEMY_HOME}/scripts/alchemake ubitrail
	${ALCHEMY_HOME}/scripts/alchemake pflash
elif [ "$target" == "mipsel" ]; then
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail
elif [ "$target" == "arm-generic" ]; then
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail
	build_mmc_tools
	build_mmc_image
elif [ "$target" == "mips-generic" ]; then
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail
elif [ "$target" == "rpi0w" ]; then
	setup_kernel_atom
	setup_uboot
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "rpi2" ]; then
	setup_kernel_atom
	setup_uboot
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "rpi3" -o "$target" == "rpi4" ]; then
	setup_kernel_atom
	setup_uboot
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "bpi-r2" ]; then
	setup_kernel_atom
	setup_uboot
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "bbb" ]; then
	setup_kernel_atom
	setup_uboot
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "mvebu" ]; then
	setup_kernel_atom
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail
	${ALCHEMY_HOME}/scripts/alchemake ubitrail
elif [ "$target" == "hikey" ]; then
	setup_kernel_atom
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "x64-uefi" ]; then
	setup_kernel_atom
	${ALCHEMY_HOME}/scripts/alchemake all
	${ALCHEMY_HOME}/scripts/alchemake final
	${ALCHEMY_HOME}/scripts/alchemake image
	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
fi
